# Code to convert pdf to txt

from PyPDF2 import PdfFileReader
#save_path= "/home/lzanella/Example_PubTator/nature_example/"
#print("Use quotation marks to give the next information, please")
print("Name of the input file (pdf): ")
input_file = input()
print("Path of the input file: ")
save_path = input()
print("Name of the output file (txt): ") #not include '.txt'
output_file = input()
x = open(save_path + output_file +".txt","wb")

def getPDFContent(path):
    content = ""
    # Load PDF into pyPDF
    pdf = PdfFileReader(save_path +input_file, "rb")
    # Iterate pages
    for i in range(0, pdf.getNumPages()):
        # Extract text from page and add to content
        content += pdf.getPage(i).extractText() + "\n"
    # Collapse whitespace
    content = " ".join(content.replace(u"\xa0", " ").strip().split())
    return content

#print(getPDFContent("test.pdf").encode("ascii", "xmlcharrefreplace"))
txt = getPDFContent("test.pdf").encode("ascii","xmlcharrefreplace")
#txt = getPDFContent("test.pdf").encode("ascii", "ignore") 
print(txt)
x.write(txt) 
x.close()

